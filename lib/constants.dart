const NETWORK_SERVICE_ID = "com.zinz.click_league";
const SERIALIZE_SEP = ';';

// All in seconds
const GAME_DURATIONS = [10, 20, 30, 60];
const DEFAULT_GAME_DURATION_ID = 0;
const COUNTDOWN_DURATION = 3;

const HIGH_SCORES_LENGTH = 10;

const PERSISTENT_USER_NAME_KEY = "my_user_name";
const DEFAULT_USER_NAME = "12FINGERZ";

const PAD = 16.0;