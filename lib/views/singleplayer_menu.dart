import 'package:flutter/material.dart';

import 'package:click_league/controllers/game_controller.dart';
import 'package:click_league/views/match_config_view.dart';
import 'package:click_league/views/game_view.dart';

class SingleplayerMenu extends StatelessWidget {
  final gameController = GameController();

  @override
  Widget build(BuildContext context) {
    return MatchConfigView(
        subtitle: "Singleplayer Match",
        submitText: "PLAY!!!",
        gameController: gameController,
        onSubmit: (context) => Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (_) => GameView(gameController: gameController))));
  }
}
