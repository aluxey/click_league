import 'package:flutter/material.dart';

import 'package:click_league/controllers/game_controller.dart';
import 'package:click_league/views/game_results_view.dart';
import 'package:click_league/views/widgets/countdown_widget.dart';
import 'package:click_league/views/widgets/scores_section.dart';
import 'package:click_league/views/widgets/timer_section.dart';
import 'package:click_league/views/widgets/my_scaffold.dart';

class GameView extends StatelessWidget {
  final GameController gameController;

  GameView({
    Key? key,
    required this.gameController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    gameController.startGame(
        onGameFinished: () => Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    GameResultsView(gameController: gameController))));

    Widget gameView = _buildGameView(context);

    return FutureBuilder(
        future: gameController.countdownFinished,
        builder: (context, snapshot) {
          if (snapshot.connectionState != ConnectionState.done) {
            return CountdownWidget(gameController: gameController);
          } else {
            return gameView;
          }
        });
  }

  Widget _buildGameView(BuildContext context) {
    return MyScaffold(
      subtitle: 'PLAY!!!',
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ScoresSection(gameController: gameController),
          TimerSection(gameController: gameController),
        ],
      ),
      footer: ElevatedButton(
          autofocus: true,
          onPressed: () => gameController.onGameButtonPressed(),
          style: ElevatedButton.styleFrom(primary: Colors.green),
          child: Text(
            "TAP FASTER!!!",
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headline3,
          )),
    );
  }
}
