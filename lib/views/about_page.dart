import 'package:flutter/material.dart';

import 'package:click_league/views/widgets/my_scaffold.dart';

class AboutPage extends StatelessWidget {
  const AboutPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      subtitle: 'About the game',
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 8.0, right: 8.0),
            child: Text.rich(TextSpan(children: [
              TextSpan(
                  text: "ClickLEAGUE",
                  style: Theme.of(context).textTheme.bodyText2),
              TextSpan(
                  text:
                      " is an experimental game, that takes interest in direct ",
                  style: Theme.of(context).textTheme.bodyText1),
              TextSpan(
                  text: "Device-to-Device (D2D) communication",
                  style: Theme.of(context).textTheme.bodyText2),
              TextSpan(
                  text: " on smartphones.\n\n",
                  style: Theme.of(context).textTheme.bodyText1),
              TextSpan(
                  text: "It currently only supports ",
                  style: Theme.of(context).textTheme.bodyText1),
              TextSpan(
                  text: "the proprietary Google Nearby protocol",
                  style: Theme.of(context).textTheme.bodyText2),
              TextSpan(
                  text: ": only usable on Android phones, works well, and ",
                  style: Theme.of(context).textTheme.bodyText1),
              TextSpan(
                  text: "makes your phone shine bright",
                  style: Theme.of(context).textTheme.bodyText2),
              TextSpan(
                  text: " signals on every wireless communication band available.\n\n",
                  style: Theme.of(context).textTheme.bodyText1),
              TextSpan(
                  text: "We intend to adapt ClickLEAGUE to other (less nosy) protocols, hoping to find a way to ",
                  style: Theme.of(context).textTheme.bodyText1),
              TextSpan(
                  text: "reliably & easily make smartphones communicate",
                  style: Theme.of(context).textTheme.bodyText2),
              TextSpan(
                  text: " among each others, ",
                  style: Theme.of(context).textTheme.bodyText1),
              TextSpan(
                  text: "without involving any third-party.\n\n",
                  style: Theme.of(context).textTheme.bodyText2),
              TextSpan(
                  text: "This work was initiated by Adrien Luxey, and funded by the Inria research center (France).",
                  style: Theme.of(context).textTheme.bodyText1),
            ])),
          ),
        ]);
  }
}
