import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:click_league/controllers/game_controller.dart';
import 'package:click_league/constants.dart';
import 'package:click_league/views/widgets/my_scaffold.dart';
import 'package:click_league/views/widgets/user_name_form.dart';

typedef MySubmitCallback = void Function(BuildContext context);

class MatchConfigView extends StatefulWidget {
  final String subtitle;
  final String submitText;
  final GameController gameController;
  final MySubmitCallback onSubmit;

  const MatchConfigView(
      {Key? key,
      required this.subtitle,
      required this.submitText,
      required this.gameController,
      required this.onSubmit})
      : super(key: key);

  @override
  _MatchConfigViewState createState() => _MatchConfigViewState();
}

class _MatchConfigViewState extends State<MatchConfigView> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  int gameDuration = GAME_DURATIONS[DEFAULT_GAME_DURATION_ID];
  String userName = "";

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      subtitle: widget.subtitle,
      body: Center(
        child: Form(
          key: formKey,
          child: Padding(
            padding: const EdgeInsets.all(PAD),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                UserNameForm(
                  onUserNameChanged: (newUserName) => userName = newUserName,
                  // formKey: formKey,
                ),
                Padding(
                  padding:
                      const EdgeInsets.fromLTRB(PAD, 2 * PAD, PAD, PAD / 4),
                  child: Text("Game duration",
                      style: Theme.of(context).textTheme.headline3),
                ),
                DropdownButton<int>(
                  value: gameDuration,
                  icon: const Icon(Icons.arrow_drop_down),
                  onChanged: (value) {
                    if (value != null) setState(() => gameDuration = value);
                  },
                  items: GAME_DURATIONS.map<DropdownMenuItem<int>>((int value) {
                    return DropdownMenuItem<int>(
                      value: value,
                      child: Text("$value s",
                          style: Theme.of(context).textTheme.bodyText1),
                    );
                  }).toList(),
                ),
              ],
            ),
          ),
        ),
      ),
      footer: ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            setState(() {
              formKey.currentState!.save();
              widget.gameController.initialise(gameDuration, userName);
              widget.onSubmit(context);
            });
          }
        },
        child: Text(
          widget.submitText,
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.headline3,
        ),
        style: ElevatedButton.styleFrom(primary: Colors.green),
      ),
    );
  }
}
