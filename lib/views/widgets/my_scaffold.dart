import 'package:flutter/material.dart';

import 'package:click_league/constants.dart';
import 'package:click_league/utils.dart';

// ignore: must_be_immutable
class MyScaffold extends StatelessWidget {
  final String subtitle;
  final Widget body;
  final Widget? footer;
  final VoidCallback? onBackPressed;
  final List<Widget>? appBarActions;

  final int _titleFlex = 1;
  late final int _bodyFlex;
  late final int _footerFlex;

  MyScaffold({
    Key? key,
    required this.subtitle,
    required this.body,
    this.footer,
    this.onBackPressed,
    this.appBarActions})
      : super(key: key) {
    if (this.footer == null) {
      _bodyFlex = 3;
      _footerFlex = 0;
    } else {
      _bodyFlex = 2;
      _footerFlex = 1;
    }
  }

  @override
  Widget build(BuildContext context) {
    var scaffold = Scaffold(
      appBar: AppBar(actions: appBarActions),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          buildTitleWidget(context),
          Expanded(
            flex: _bodyFlex,
            child: Container(
              color: Colors.black,
              child: this.body
            ),
          ),
          Expanded(
            flex: _footerFlex,
            child: Container(
              color: Colors.black,
              child: this.footer
            ),
          ),
        ],
      ),
    );

    if (onBackPressed == null)
      return scaffold;
    else {
      return WillPopScope(
        onWillPop: () async {
          onBackPressed!();
          return true;
        },
        child: scaffold,
      );
    }
  }

  Widget buildTitleWidget(BuildContext context) {
    return Expanded(
        flex: _titleFlex,
        child: Container(
          color: Colors.black,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: PAD*2),
                child: Center(
                    child: Text("ClickLEAGUE",
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.headline1)),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: PAD*3/4),
                child: Center(
                    child: Text(subtitle,
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.headline3)),
              ),
            ]),
        ));
  }
}
