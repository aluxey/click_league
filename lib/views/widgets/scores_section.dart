import 'package:flutter/material.dart';

import 'package:click_league/constants.dart';
import 'package:click_league/controllers/game_controller.dart';
import 'package:click_league/models/user_model.dart';
import 'package:click_league/views/widgets/my_circular_progress_indicator.dart';
import 'package:click_league/views/widgets/user_score.dart';

class ScoresSection extends StatelessWidget {
  final GameController gameController;

  ScoresSection({Key? key, required this.gameController}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
            padding: const EdgeInsets.fromLTRB(PAD, PAD * 2, PAD, PAD / 4),
            child:
                Text("Scores", style: Theme.of(context).textTheme.headline3)),
        Padding(
          padding: const EdgeInsets.fromLTRB(PAD, PAD / 2, PAD, PAD),
          child: _buildUsersScoreList(context),
        ),
      ],
    );
  }

  Widget _buildUsersScoreList(BuildContext context) {
    return StreamBuilder<List<UserModel>>(
        stream: gameController.usersStream,
        initialData: gameController.users,
        builder: (context, snapshot) {
          List<Widget>? children;
          if (snapshot.hasData &&
              !snapshot.hasError &&
              (snapshot.connectionState == ConnectionState.active ||
                  snapshot.connectionState == ConnectionState.waiting &&
                      snapshot.data != null)) {
            List<UserModel> users = snapshot.data!;
            children = List<Widget>.generate(users.length,
                (i) => UserScore(user: users[i], isMyUser: i == 0));
          } else {
            // We don't have a Users list yet
            children = [MyCircularProgressIndicator()];
          }
          return Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: children);
        });
  }
}
