import 'package:flutter/material.dart';

import 'package:click_league/constants.dart';
import 'package:click_league/models/user_model.dart';

class UserScore extends StatelessWidget {
  final bool isMyUser, isFinal;
  final UserModel user;

  const UserScore(
      {Key? key,
      required this.user,
      required this.isMyUser,
      this.isFinal = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextStyle scoreStyle = isMyUser
        ? Theme.of(context).textTheme.bodyText2!
        : Theme.of(context).textTheme.bodyText1!;
    String userName = isMyUser ? user.userName + " (me)" : user.userName;

    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: PAD / 2),
          child: Text(userName, style: Theme.of(context).textTheme.subtitle1),
        ),
        isFinal
            ? Text(user.score.toString(), style: scoreStyle)
            : StreamBuilder<int>(
                stream: user.scoreStream,
                builder: (context, snapshot) {
                  if (!snapshot.hasData || snapshot.hasError) {
                    return Text("n/a", style: scoreStyle);
                  } else {
                    switch (snapshot.connectionState) {
                      case ConnectionState.none:
                      case ConnectionState.waiting:
                      case ConnectionState.done:
                        return Text("n/a", style: scoreStyle);
                      case ConnectionState.active:
                        return Text(snapshot.data!.toString(),
                            style: scoreStyle);
                    }
                  }
                }),
      ],
    );
  }
}
