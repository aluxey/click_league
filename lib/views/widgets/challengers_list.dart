import 'package:flutter/material.dart';

import 'package:click_league/constants.dart';
import 'package:click_league/models/user_model.dart';
import 'package:click_league/controllers/game_controller.dart';
import 'package:click_league/views/widgets/my_circular_progress_indicator.dart';

class ChallengersList extends StatelessWidget {
  final GameController gameController;

  const ChallengersList({Key? key, required this.gameController})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<UserModel>>(
        stream: gameController.usersStream,
        initialData: gameController.users,
        builder: (context, snapshot) => Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  _buildUsersListTitle(context, snapshot),
                  _buildUsersList(context, snapshot),
                ]));
  }

  Widget _buildUsersListTitle(
      BuildContext context, AsyncSnapshot<List<UserModel>> snapshot) {
    String text = "No fwiends yet :(";
    if (snapshot.hasData &&
        !snapshot.hasError &&
        (snapshot.connectionState == ConnectionState.active ||
            snapshot.connectionState == ConnectionState.waiting &&
                snapshot.data != null)) {
      text = "${snapshot.data!.length} challenger(s)...";
    }

    return Padding(
      padding: const EdgeInsets.fromLTRB(PAD, PAD, PAD, PAD / 4),
      child: Text(text, style: Theme.of(context).textTheme.headline3),
    );
  }

  Widget _buildUsersList(
      BuildContext context, AsyncSnapshot<List<UserModel>> snapshot) {
    var children = List<Widget>.empty();

    if (snapshot.hasData &&
        !snapshot.hasError &&
        (snapshot.connectionState == ConnectionState.active ||
            snapshot.connectionState == ConnectionState.waiting &&
                snapshot.data != null)) {
      children = snapshot.data!.map((u) => _buildUserRow(context, u)).toList();
    }

    // Always add a circular progress indicator
    children = children + [MyCircularProgressIndicator()];

    return Container(
        padding: const EdgeInsets.all(PAD / 2),
        decoration: BoxDecoration(border: Border.all(color: Colors.white70)),
        child: ListView.separated(
            shrinkWrap: true,
            separatorBuilder: (_, __) => const Divider(color: Colors.white70),
            itemCount: children.length,
            itemBuilder: (_, i) => children[i]));
  }

  Widget _buildUserRow(BuildContext context, UserModel u) {
    var userText = u.userName;
    if (u.id == gameController.myUser.id) userText += " (me)";

    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.only(
              left: PAD / 2, right: PAD / 2, bottom: PAD / 2),
          child: Icon(Icons.person),
        ),
        Text(userText, style: Theme.of(context).textTheme.bodyText1),
      ],
    );
  }
}
