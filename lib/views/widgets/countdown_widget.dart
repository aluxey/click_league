import 'package:flutter/material.dart';

import 'package:click_league/controllers/game_controller.dart';
import 'package:click_league/constants.dart';
import 'package:click_league/views/widgets/my_scaffold.dart';

class CountdownWidget extends StatelessWidget {
  final GameController gameController;

  CountdownWidget({Key? key, required this.gameController}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
        subtitle: "Countdown...",
        body: Center(child: _buildTimeLeft(context)),
        footer: ElevatedButton(
            onPressed: null, // button will be disabled
            style: ElevatedButton.styleFrom(primary: Colors.green),
            child: Text(
              "Get ready to tap...",
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headline3,
            )));
  }

  Widget _buildTimeLeft(BuildContext context) {
    // final double remainingCountdownSeconds =
    //     gameTimer.countdownMilliseconds / 1000.0;
    // Text(remainingCountdownSeconds.toStringAsFixed(1),
    //           style: Theme.of(context).textTheme.headline1),
    TextStyle? style = Theme.of(context).textTheme.headline1;
    return StreamBuilder<int>(
        stream: gameController.countdownStream,
        initialData: COUNTDOWN_DURATION * 1000,
        builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
          if (snapshot.hasError) {
            return Text("DOH", style: style);
          } else {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
              case ConnectionState.waiting:
                return Text("DOH", style: style);
              case ConnectionState.active:
                double remainingSeconds = snapshot.data! / 1000.0;
                return Text(remainingSeconds.toStringAsFixed(1), style: style);
              case ConnectionState.done:
                return Text("GO!!!", style: style);
            }
          }
        });
  }
}
