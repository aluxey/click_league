import 'package:flutter/material.dart';

import 'package:click_league/controllers/game_controller.dart';
import 'package:click_league/constants.dart';

class TimerSection extends StatelessWidget {
  final GameController gameController;

  TimerSection({
    Key? key,
    required this.gameController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
              padding: const EdgeInsets.fromLTRB(PAD, 2 * PAD, PAD, PAD / 4),
              child: Text("Time left",
                  style: Theme.of(context).textTheme.headline3)),
          Padding(
            padding: const EdgeInsets.fromLTRB(PAD, PAD / 2, PAD, PAD),
            child: _buildTimeLeft(context),
          ),
        ],
      ),
    );
  }

  Widget _buildTimeLeft(BuildContext context) {
    TextStyle? style = Theme.of(context).textTheme.headline1;
    return StreamBuilder<int>(
        stream: gameController.gameTimerStream,
        builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
          if (!snapshot.hasData || snapshot.hasError) {
            return Text(gameController.gameDuration.toStringAsFixed(1), style: style);
          } else {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
              case ConnectionState.waiting:
                return Text("DOH", style: style);
              case ConnectionState.active:
                double remainingSeconds = snapshot.data! / 1000.0;
                return Text(remainingSeconds.toStringAsFixed(1), style: style);
              case ConnectionState.done:
                return Text("0.0", style: style);
            }
          }
        });
  }
}
