import 'package:flutter/material.dart';

import 'package:click_league/constants.dart';
import 'package:click_league/models/models.dart';
import 'package:click_league/views/widgets/buttons.dart';
import 'package:click_league/views/widgets/my_circular_progress_indicator.dart';

typedef OnJoinGameCallback = void Function(
    BuildContext context, String endpointId);

class AvailableGames extends StatelessWidget {
  final OnJoinGameCallback onJoinGame;
  final Stream<Map<String, GameDescription>> availableGamesStream;

  const AvailableGames(
      {Key? key, required this.availableGamesStream, required this.onJoinGame})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(PAD, PAD, PAD, 4.0),
            child: Text("Available Matches",
                style: Theme.of(context).textTheme.headline3),
          ),
          Container(
              padding: const EdgeInsets.all(8.0),
              decoration:
                  BoxDecoration(border: Border.all(color: Colors.white70)),
              child: _buildAvailableGamesList(context))
        ]);
  }

  Widget _buildAvailableGamesList(BuildContext context) {
    return StreamBuilder<Map<String, GameDescription>>(
        stream: availableGamesStream,
        builder: (context, snapshot) {
          var children = List<Widget>.empty();
          if (snapshot.hasData &&
              !snapshot.hasError &&
              snapshot.connectionState == ConnectionState.active) {
            Map<String, GameDescription> games = snapshot.data!;
            children = games
                .map<String, Widget>((endpointId, game) => MapEntry(endpointId,
                    _buildGameDescription(context, endpointId, game)))
                .values
                .toList();
          }

          // Always add a circular progress indicator
          children = children + [MyCircularProgressIndicator()];

          return ListView.separated(
              shrinkWrap: true,
              separatorBuilder: (_, __) => const Divider(
                    color: Colors.white70,
                  ),
              itemCount: children.length,
              itemBuilder: (_, i) => children[i]);
        });
  }

  Widget _buildGameDescription(
      BuildContext context, String endpointId, GameDescription gd) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Padding(
                  padding:
                      const EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0),
                  child: Icon(Icons.person),
                ),
                Text('${gd.userName}',
                    style: Theme.of(context).textTheme.bodyText1),
              ],
            ),
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                  child: Icon(Icons.access_alarm),
                ),
                Text('${gd.gameDuration} s',
                    style: Theme.of(context).textTheme.bodyText1),
              ],
            )
          ],
        ),
        MenuButton(
          text: "Join >",
          onPressed: () => onJoinGame(context, endpointId),
        )
      ],
    );
  }
}
