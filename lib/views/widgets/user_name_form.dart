import 'package:flutter/material.dart';

import 'package:click_league/constants.dart';
import 'package:click_league/models/models.dart';
import 'package:click_league/views/widgets/my_circular_progress_indicator.dart';

typedef UserNameChangedCallback = void Function(String);

class UserNameForm extends StatefulWidget {
  final UserNameChangedCallback onUserNameChanged;
  const UserNameForm({Key? key, required this.onUserNameChanged})
      : super(key: key);

  @override
  _UserNameFormState createState() => _UserNameFormState();
}

class _UserNameFormState extends State<UserNameForm> {
  
  bool initialUserNameLoaded = false;
  String initialUserName = "";

  @override
  void initState() {
    super.initState();
    getLastUserName().then((lastUserName) {
      setState(() {
        initialUserName = lastUserName;
        initialUserNameLoaded = true;
      });
      // Call the callback once we have a username
      widget.onUserNameChanged(initialUserName);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(PAD, 0, PAD, PAD/4),
            child:
                Text("Pseudonym", style: Theme.of(context).textTheme.headline3),
          ),
          initialUserNameLoaded
              ? TextFormField(
                  decoration: const InputDecoration(
                    icon: Icon(Icons.person),
                  ),
                  style: Theme.of(context).textTheme.bodyText1,
                  initialValue: initialUserName,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'One needs a pseudonym!';
                    }
                    return null;
                  },
                  onChanged: (newUserName) async {
                    widget.onUserNameChanged(newUserName);
                    await setLastUserName(newUserName);
                  },
                )
              : const MyCircularProgressIndicator(),
        ]);
  }
}
