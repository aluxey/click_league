import 'package:flutter/material.dart';

import 'package:click_league/constants.dart';
import 'package:click_league/controllers/game_controller.dart';
import 'package:click_league/models/user_model.dart';
import 'package:click_league/views/widgets/buttons.dart';
import 'package:click_league/views/widgets/my_circular_progress_indicator.dart';
import 'package:click_league/views/widgets/my_scaffold.dart';
import 'package:click_league/views/widgets/user_score.dart';

class GameResultsView extends StatelessWidget {
  final GameController gameController;

  GameResultsView({
    Key? key,
    required this.gameController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      subtitle: 'Game Finished!',
      body: _buildGameResultsBodyOnceReceived(context),
      footer: null, // Spans the scores until the bottom of the screen
    );
  }

  Widget _buildGameResultsBodyOnceReceived(BuildContext context) {
    return FutureBuilder(
        future: gameController.finalResultsReceived,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return _buildGameResultsBody(context);
          } else {
            return _buildAwaitingResultsBody(context);
          }
        });
  }

  Widget _buildAwaitingResultsBody(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        MyCircularProgressIndicator(),
        Center(
            child: Text("Awaiting results...",
                style: Theme.of(context).textTheme.bodyText1)),
      ],
    );
  }

  Widget _buildGameResultsBody(BuildContext context) {
    List<UserModel> users = gameController.users;
    // Sort by score descending (TODO: very order)

    Widget? usersListWidget;
    if (users.length >= 2) {
      users.sort((u1, u2) => u2.score - u1.score);
      usersListWidget = Expanded(
          child: GridView.count(
              crossAxisCount: 2,
              mainAxisSpacing: PAD,
              crossAxisSpacing: PAD,
              padding: const EdgeInsets.all(PAD),
              children: List<Widget>.generate(
                  users.length,
                  (i) => UserScore(
                      user: users[i],
                      isMyUser: users[i].id == gameController.myUser.id,
                      isFinal: true))));
    } else {
      usersListWidget =
          UserScore(user: users[0], isMyUser: true, isFinal: true);
    }

    return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          MenuButton(
            text: "Main menu",
            onPressed: () =>
                Navigator.popUntil(context, ModalRoute.withName('/main-menu')),
          ),
          Padding(
              padding: const EdgeInsets.fromLTRB(PAD, PAD * 2, PAD, PAD / 4),
              child: Text("Final Scores",
                  style: Theme.of(context).textTheme.headline3)),
          usersListWidget,
        ]);
  }
}
