import 'package:click_league/views/multiplayer_settings_menu.dart';
import 'package:flutter/material.dart';

import 'package:click_league/views/widgets/buttons.dart';
import 'package:click_league/views/widgets/my_scaffold.dart';
import 'package:click_league/views/about_page.dart';
import 'package:click_league/views/high_scores_view.dart';
import 'package:click_league/views/singleplayer_menu.dart';

class MainMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      subtitle: "Main Menu",
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            MenuButton(
              text: "Singleplayer",
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SingleplayerMenu()));
              },
            ),
            MenuButton(
              text: "Multiplayer",
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => MultiplayerSettingsMenu()));
              },
            ),
            MenuButton(
              text: "High Scores",
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => HighScoresView()));
              },
            ),
            MenuButton(
              text: "About the game",
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AboutPage()));
              },
            ),
          ],
        ),
      ),
    );
  }
}

