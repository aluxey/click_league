import 'package:flutter/material.dart';

import 'package:click_league/constants.dart';
import 'package:click_league/models/models.dart';
import 'package:click_league/utils.dart';
import 'package:click_league/views/widgets/my_circular_progress_indicator.dart';
import 'package:click_league/views/widgets/my_scaffold.dart';

class HighScoresView extends StatefulWidget {
  const HighScoresView({Key? key}) : super(key: key);

  @override
  _HighScoresViewState createState() => _HighScoresViewState();
}

class _HighScoresViewState extends State<HighScoresView> {
  HighScoresModel _dbHandler = HighScoresModel();

  @override
  void initState() {
    super.initState();
    _dbHandler.init();
  }

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
        subtitle: "High Scores",
        appBarActions: [_buildClearHighScoresButton(context)],
        body: Center(
            child: DefaultTabController(
                length: GAME_DURATIONS.length,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _buildTabBar(context),
                    Expanded(
                        child: TabBarView(children: _buildTabsContent(context)))
                  ],
                ))));
  }

  Widget _buildClearHighScoresButton(BuildContext context) {
    return IconButton(
        icon: const Icon(Icons.delete_outlined),
        tooltip: 'Clear high scores',
        onPressed: () {
          showDialog(
            context: context,
            builder: (context) =>
                _buildClearHighScoresConfirmationPopUp(context),
          );
        });
  }

  Widget _buildClearHighScoresConfirmationPopUp(BuildContext context) {
    return new AlertDialog(
      title:
          Text('Are you sure?', style: Theme.of(context).textTheme.subtitle1!),
      content: new Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text("Do you really want to clear all high scores?",
              style: Theme.of(context).textTheme.bodyText1!),
        ],
      ),
      actions: <Widget>[
        new TextButton(
            onPressed: () => Navigator.of(context).pop(),
            child:
                Text('Cancel', style: Theme.of(context).textTheme.bodyText1!)),
        new TextButton(
            onPressed: () {
              _dbHandler.clearHighScores().then((nDeleted) => showSnackbar(context, "Deleted $nDeleted high scores"));
              setState(() => Navigator.of(context).pop());
            },
            child: Text('Clear high scores',
                style: Theme.of(context).textTheme.bodyText1!)),
      ],
    );
  }

  Widget _buildTabBar(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.only(right: 8.0),
          child: Icon(Icons.access_alarm),
        ),
        Expanded(
            child: TabBar(
                tabs: GAME_DURATIONS
                    .map((duration) => Tab(
                          child: Text('$duration s',
                              style: Theme.of(context).textTheme.bodyText1),
                        ))
                    .toList()))
      ],
    );
  }

  List<Widget> _buildTabsContent(BuildContext context) {
    return GAME_DURATIONS
        .map((duration) => _buildHighScoreList(context, duration))
        .toList();
  }

  Widget _buildHighScoreList(BuildContext context, int gameDuration) {
    return FutureBuilder<List<HighScore>>(
        future: _dbHandler.selectHighScores(gameDuration: gameDuration),
        builder: (context, snapshot) {
          Widget ret;
          if (snapshot.hasData) {
            var l = snapshot.data!;
            if (l.length == 0) {
              ret = Text("No high scores 😴",
                  style: Theme.of(context).textTheme.bodyText1);
            } else {
              ret = Container(
                  padding: const EdgeInsets.all(8.0),
                  decoration:
                      BoxDecoration(border: Border.all(color: Colors.white70)),
                  child: ListView.separated(
                    shrinkWrap: true,
                    itemCount: l.length,
                    itemBuilder: (context, index) =>
                        HighScoreWidget(highScore: l[index]),
                    separatorBuilder: (context, index) => const Divider(
                      color: Colors.white70,
                    ),
                  ));
            }
          } else if (snapshot.hasError) {
            ret = Text("Error retrieving high scores: ${snapshot.error}",
                style: Theme.of(context).textTheme.bodyText1);
          } else {
            ret = MyCircularProgressIndicator();
          }

          return Center(child: ret);
        });
  }
}

class HighScoreWidget extends StatelessWidget {
  final HighScore highScore;

  const HighScoreWidget({Key? key, required this.highScore}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget topRow = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Row(
          children: [
            Padding(
              padding:
                  const EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0),
              child: Icon(Icons.person),
            ),
            Text('${highScore.userName}',
                style: Theme.of(context).textTheme.bodyText1),
          ],
        ),
        Row(
          children: [
            Text('${highScore.score}',
                style: Theme.of(context).textTheme.bodyText1),
            Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              child: Icon(Icons.flag),
            ),
          ],
        ),
      ],
    );

    Widget dateWidget = Row(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 8.0, right: 8.0),
          child: Icon(Icons.calendar_today),
        ),
        Text(highScore.printDate(),
            style: Theme.of(context).textTheme.bodyText1),
      ],
    );
    Widget bottomRow = dateWidget;
    if (!highScore.isLocal) {
      bottomRow = Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          dateWidget,
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Text("remote", style: Theme.of(context).textTheme.bodyText1),
          )
        ],
      );
    }

    return Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [topRow, bottomRow]);
  }
}
