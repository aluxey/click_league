import 'package:flutter/material.dart';

import 'package:click_league/constants.dart';
import 'package:click_league/utils.dart';
import 'package:click_league/controllers/network/network_controller.dart';
import 'package:click_league/controllers/network/guest_controller.dart';
import 'package:click_league/controllers/network/host_controller.dart';
import 'package:click_league/views/multiplayer_guest_menu.dart';
import 'package:click_league/views/multiplayer_host_menu.dart';
import 'package:click_league/views/widgets/my_scaffold.dart';

class MultiplayerSettingsMenu extends StatefulWidget {
  const MultiplayerSettingsMenu({Key? key}) : super(key: key);

  @override
  State<MultiplayerSettingsMenu> createState() =>
      _MultiplayerSettingsMenuState();
}

class _MultiplayerSettingsMenuState extends State<MultiplayerSettingsMenu> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  NetworkChannelType? _selectedChannel;
  NetworkRole? _selectedRole;

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      subtitle: "Network settings",
      body: Center(
        child: Form(
          key: formKey,
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(PAD, 0, PAD, PAD / 4),
                  child: Text("Network channel",
                      style: Theme.of(context).textTheme.headline3),
                ),
                DropdownButton<NetworkChannelType>(
                  value: _selectedChannel,
                  icon: const Icon(Icons.arrow_drop_down),
                  onChanged: (value) {
                    if (value != null) setState(() => _selectedChannel = value);
                  },
                  items: List<DropdownMenuItem<NetworkChannelType>>.generate(
                      NetworkChannelTypeString.length,
                      (i) => DropdownMenuItem<NetworkChannelType>(
                            value: NetworkChannelTypeString.keys.elementAt(i),
                            child: Text(
                                NetworkChannelTypeString.values.elementAt(i),
                                style: Theme.of(context).textTheme.bodyText1),
                          )),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(PAD, PAD, PAD, PAD / 4),
                  child: Text("Role",
                      style: Theme.of(context).textTheme.headline3),
                ),
                DropdownButton<NetworkRole>(
                  value: _selectedRole,
                  icon: const Icon(Icons.arrow_drop_down),
                  onChanged: (value) {
                    if (value != null) setState(() => _selectedRole = value);
                  },
                  items: List<DropdownMenuItem<NetworkRole>>.generate(
                      NetworkRoleString.length,
                      (i) => DropdownMenuItem<NetworkRole>(
                            value: NetworkRoleString.keys.elementAt(i),
                            child: Text(NetworkRoleString.values.elementAt(i),
                                style: Theme.of(context).textTheme.bodyText1),
                          )),
                ),
              ]),
        ),
      ),
      footer: ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            setState(() {
              formKey.currentState!.save();
              _onFormValidated(context);
            });
          }
        },
        child: Text(
          "Confirm",
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.headline3,
        ),
        style: ElevatedButton.styleFrom(primary: Colors.green),
      ),
    );
  }

  Future<void> _onFormValidated(BuildContext context) async {
    if (_selectedChannel == null) {
      showSnackbar(context, "Please select a communication channel.");
      return;
    }
    if (_selectedRole == null) {
      showSnackbar(context, "Please select a role.");
      return;
    }

    try {
      var netController = NetworkController(_selectedChannel!, _selectedRole!);
      if (await netController.acquirePermissions()) {
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) {
          switch (_selectedRole!) {
            case NetworkRole.GUEST:
              return MultiplayerGuestMenu(
                  netController: netController as GuestController);
            case NetworkRole.HOST:
              return MultiplayerHostMenu(
                  netController: netController as HostController);
          }
        }));
      }
    } catch (e) {
      showSnackbar(context, "Failed: $e");
    }
  }
}
