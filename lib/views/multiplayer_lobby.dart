import 'package:flutter/material.dart';

import 'package:click_league/controllers/game_controller.dart';
import 'package:click_league/views/game_view.dart';
import 'package:click_league/views/widgets/challengers_list.dart';
import 'package:click_league/views/widgets/my_scaffold.dart';

// The Lobby is the room inside which one waits for players before starting a match
class MultiplayerLobby extends StatelessWidget {
  final GameController gameController;

  MultiplayerLobby({Key? key, required this.gameController}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      subtitle: gameController.isHost
          ? "Advertising game..."
          : "Waiting for opponents...",
      body: ChallengersList(gameController: gameController),
      footer: gameController.isHost ? _buildFooterButton(context) : null,
    );
  }

  Widget _buildFooterButton(BuildContext context) {
    assert(gameController.isHost);

    return ElevatedButton(
      onPressed: () {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (_) => GameView(gameController: gameController)));
      },
      child: Text(
        "PLAY!!!",
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.headline3,
      ),
      style: ElevatedButton.styleFrom(primary: Colors.green),
    );
  }
}
