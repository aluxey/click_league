import 'package:click_league/utils.dart';
import 'package:flutter/material.dart';

import 'package:click_league/controllers/game_controller.dart';
import 'package:click_league/controllers/network/guest_controller.dart';
import 'package:click_league/views/game_view.dart';
import 'package:click_league/views/multiplayer_lobby.dart';
import 'package:click_league/views/widgets/available_games.dart';
import 'package:click_league/views/widgets/my_scaffold.dart';
import 'package:click_league/views/widgets/user_name_form.dart';

class MultiplayerGuestMenu extends StatefulWidget {
  final GameController gameController;
  final GuestController netController;

  MultiplayerGuestMenu({Key? key, required this.netController})
      : gameController = GameController(netController: netController),
        super(key: key) {
    netController.discoverGames(gameController);
  }

  @override
  State<MultiplayerGuestMenu> createState() => _MultiplayerGuestMenuState();
}

class _MultiplayerGuestMenuState extends State<MultiplayerGuestMenu> {
  String _myUserName = '';

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
        subtitle: 'Browse Matches',
        body: Column(
          children: [
            UserNameForm(
                onUserNameChanged: (userName) => _myUserName = userName),
            AvailableGames(
                availableGamesStream: widget.netController.availableGamesStream,
                onJoinGame: _onJoinGame)
          ],
        ));
  }

  Future<void> _onJoinGame(BuildContext context, String endpointId) async {
    widget.gameController.initialiseUser(_myUserName);
    try {
      await widget.netController.joinGame(endpointId,
          onGameJoined: () => Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => MultiplayerLobby(
                      gameController: widget.gameController))),
          onGameStarts: () => Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (_) =>
                      GameView(gameController: widget.gameController))));
    } catch (exception) {
      showSnackbar(context, "Failed joining game: $exception");
    }
  }
}
