import 'package:flutter/material.dart';

import 'package:click_league/controllers/game_controller.dart';
import 'package:click_league/controllers/network/host_controller.dart';
import 'package:click_league/views/match_config_view.dart';
import 'package:click_league/views/multiplayer_lobby.dart';

class MultiplayerHostMenu extends StatelessWidget {
  final GameController gameController;
  final HostController netController;

  MultiplayerHostMenu({Key? key, required this.netController})
      : gameController = GameController(netController: netController),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return MatchConfigView(
      subtitle: "Host Match",
      submitText: "Propose",
      gameController: gameController,
      onSubmit: (context) {
        netController.advertiseGame(gameController);
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => MultiplayerLobby(
                  gameController: gameController)),
        );
      },
    );
  }
}
