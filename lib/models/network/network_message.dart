import 'dart:typed_data';

import 'package:click_league/models/user_model.dart';

enum NetworkMessageType { USER, USERS_LIST, PLAY, FINAL_USER, FINAL_USERS_LIST }

class NetworkMessage {
  final NetworkMessageType type;
  final Uint8List? bytes;

  const NetworkMessage(this.type, this.bytes);
  NetworkMessage.fromString(this.type, String s)
      : bytes = Uint8List.fromList(s.codeUnits);

  static const PLAY = NetworkMessage(NetworkMessageType.PLAY, null);

  dynamic get payload {
    if (type == NetworkMessageType.USER ||
        type == NetworkMessageType.FINAL_USER)
      return UserModel.deserialize(String.fromCharCodes(bytes!));
    else if (type == NetworkMessageType.USERS_LIST ||
        type == NetworkMessageType.FINAL_USERS_LIST)
      return deserializeUsersList(String.fromCharCodes(bytes!));
    else
      return null;
  }

  Uint8List serialize() {
    /*
    Serialized format:
         +---------------------------------+
    len: | 1 B  | 4 B (int32) | bytesLen B |
    var: | type |  bytesLen   |   bytes    |
         +---------------------------------+
     TODO: Use ASN.1 data structure serialisation format instead of custom.
     https://pub.dev/packages/asn1lib
     */
    int bytesLen = 0;
    if (bytes != null) bytesLen = bytes!.lengthInBytes;

    var ret = BytesBuilder(copy: true);

    // TYPE
    ret.addByte(type.index);

    // BYTES_LEN
    var bytesLenBuffer = ByteData(4);
    bytesLenBuffer.setUint32(0, bytesLen, Endian.big);
    ret.add(Uint8List.sublistView(bytesLenBuffer));

    // BYTES
    if (bytes != null) ret.add(bytes!);

    return ret.takeBytes();
  }

  static NetworkMessage deserialize(Uint8List bytes) {
    assert(bytes.lengthInBytes >= 5);

    var messageType = NetworkMessageType.values[bytes[0]];
    var bytesLen =
        ByteData.sublistView(bytes.sublist(1, 5)).getUint32(0, Endian.big);

    Uint8List? messageBytes;
    if (bytesLen > 0) messageBytes = bytes.sublist(5);

    var ret = NetworkMessage(
      messageType,
      messageBytes,
    );

    if (bytesLen > 0) {
      assert(ret.bytes != null);
      assert(bytesLen == ret.bytes!.lengthInBytes);
    }
    return ret;
  }
}
