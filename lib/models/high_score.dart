import 'package:intl/intl.dart';

import 'package:click_league/models/user_model.dart';

class HighScore {
  final int? id;
  final String userName;
  final int score;
  final int gameDuration;
  final DateTime timestamp;
  final bool isLocal; // Is it a local high score (true), or remote (false)?

  HighScore({
    this.id,
    required this.userName,
    required this.score,
    required this.gameDuration,
    required this.timestamp,
    required this.isLocal,
  });

  HighScore.fromUser(
      {required UserModel user,
      required this.gameDuration,
      required this.isLocal})
      : id = null,
        userName = user.userName,
        score = user.score,
        timestamp = DateTime.now().toUtc();

  HighScore.fromMap(Map<String, dynamic> m)
      : id = m['id'],
        userName = m['user_name'],
        score = m['score'],
        gameDuration = m['game_duration'],
        timestamp = DateTime.parse(m['timestamp']),
        isLocal = m['is_local'] == 1 ? true : false;

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'user_name': userName,
      'score': score,
      'game_duration': gameDuration,
      'timestamp': timestamp.toUtc().toIso8601String(),
      'is_local': isLocal ? 1 : 0,
    };
  }

  final _dateFormatter = DateFormat('d/MM/y H:m');

  String printDate() => _dateFormatter.format(timestamp.toLocal());

  @override
  bool operator ==(Object other) {
    return other is HighScore &&
        userName == other.userName &&
        score == other.score &&
        gameDuration == other.gameDuration &&
        timestamp == other.timestamp;
  }

  @override
  int get hashCode =>
      userName.hashCode + score + gameDuration + timestamp.hashCode;
}

const sql_high_scores_table_name = 'high_scores';

const sql_create_table_high_scores = '''
CREATE TABLE $sql_high_scores_table_name(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  user_name TEXT,
  score INTEGER,
  game_duration INTEGER,
  timestamp TEXT,
  is_local INTEGER DEFAULT 1
)''';
