import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'package:click_league/constants.dart';

import 'models.dart';

class HighScoresModel {
  Database? _db;

  Future<void> init() async {
    if (_db == null)
      _db = await openDatabase(
        join(await getDatabasesPath(), 'click_league.db'),
        onCreate: (database, version) async {
          await database.execute(sql_create_table_high_scores);
        },
        version: 1,
      );
  }

  Future<List<HighScore>> selectHighScores({required int gameDuration}) async {
    await init();

    List<Map<String, dynamic>> maps = await _db!.query(
      sql_high_scores_table_name,
      distinct: true,
      limit: HIGH_SCORES_LENGTH,
      where: 'game_duration == ?',
      whereArgs: [gameDuration],
      orderBy: 'score DESC',
    );

    // Prune other high scores, but don't await before returning
    var highScoresId = List<int>.generate(maps.length, (i) => maps[i]['id']);
    pruneHighScores(gameDuration: gameDuration, idsToKeep: highScoresId);

    return List.generate(maps.length, (i) => HighScore.fromMap(maps[i]));
  }

  Future<void> insertHighScore(HighScore userScore) async {
    await init();
    await _db!.insert(
      sql_high_scores_table_name,
      userScore.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> insertScores(List<UserModel> scores, int gameDuration, int myId) async {
    for (UserModel u in scores) {
      await insertHighScore(HighScore.fromUser(
          user: u,
          gameDuration: gameDuration,
          isLocal: u.id == myId));
    }
  }

  Future<int> clearHighScores() async {
    await init();
    // Returns the number of rows deleted
    var nDeleted = await _db!.delete(sql_high_scores_table_name);
    return nDeleted;
  }

  // Delete old high scores
  Future<int> pruneHighScores(
      {required int gameDuration, required List<int> idsToKeep}) async {
    if (idsToKeep.length == 0) return 0;

    await init();

    var where = 'game_duration == $gameDuration';
    where += ' AND ';
    where += 'id NOT IN (${idsToKeep.map((id) => '$id').join(',')})';

    var nDeleted = await _db!.delete(sql_high_scores_table_name, where: where);

    return nDeleted;
  }
}
