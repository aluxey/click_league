import 'package:click_league/constants.dart';

class GameDescription {
  final String userName;
  final int gameDuration;
  final int id;

  GameDescription(this.id, this.userName, this.gameDuration);

  String serialize() {
    return id.toString() +
        SERIALIZE_SEP +
        userName +
        SERIALIZE_SEP +
        gameDuration.toString();
  }

  static GameDescription deserialize(String s) {
    if (s.length == 0)
      throw FormatException("Empty serialized GameDescription");

    var chunks = s.split(SERIALIZE_SEP);
    if (chunks.length != 3)
      throw FormatException(
          "Invalid number of chunks in serialized GameDescription");

    return GameDescription(
        int.parse(chunks[0]), chunks[1], int.parse(chunks[2]));
  }
}
