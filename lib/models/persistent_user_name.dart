import 'package:shared_preferences/shared_preferences.dart';

import 'package:click_league/constants.dart';

Future<String> getLastUserName() async {
  final prefs = await SharedPreferences.getInstance();

  return prefs.getString(PERSISTENT_USER_NAME_KEY) ?? DEFAULT_USER_NAME;
}

Future<void> setLastUserName(String lastUserName) async {
  final prefs = await SharedPreferences.getInstance();
  await prefs.setString(PERSISTENT_USER_NAME_KEY, lastUserName);
}
