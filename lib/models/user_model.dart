import 'dart:async';
import 'dart:math' show Random;

import 'package:click_league/constants.dart';

class UserModel {
  static const int N_SERIALIZED_CHUNKS = 3;
  final int _id;
  String _userName;
  int _score;
  // The stream is listened to twice: in-game, and for the GameResults => broadcast
  final _scoreStreamController = StreamController<int>.broadcast();
  late final bool _easter;

  UserModel(this._id, this._userName, this._score) {
    _isEaster();
    _scoreStreamController.add(_score);
  }

  UserModel.fromUserName(this._userName)
      : _id = Random().nextInt(1 << 32),
        _score = 0 {
    _isEaster();
    _scoreStreamController.add(_score);
  }

  int get id => _id;

  int get score => _score;

  String get userName => _userName;

  Stream<int> get scoreStream => _scoreStreamController.stream;

  void incrementScore({int value: 1}) {
    _score += _easter ? -1 * value : value;
    _scoreStreamController.add(_score);
  }

  void setScore(int score) {
    _score = score;
    _scoreStreamController.add(_score);
  }

  String serialize() {
    return id.toString() +
        SERIALIZE_SEP +
        _userName +
        SERIALIZE_SEP +
        _score.toString();
  }

  static UserModel deserialize(String s) {
    if (s.length == 0) throw FormatException("Empty serialized User");

    var chunks = s.split(SERIALIZE_SEP);
    return UserModel.deserializeChunks(chunks);
  }

  static UserModel deserializeChunks(List<String> chunks) {
    if (chunks.length != N_SERIALIZED_CHUNKS)
      throw FormatException("Invalid number of chunks for a serialized User");

    return UserModel(int.parse(chunks[0]), chunks[1], int.parse(chunks[2]));
  }

  void _isEaster() => _easter = (_userName.toLowerCase() == "macron");

  void dispose() {
    if (!_scoreStreamController.isClosed) _scoreStreamController.close();
  }
}

String serializeUsersList(List<UserModel> users) {
  String ret = "";
  assert(users.length != 0);

  // users
  for (var u in users) ret += u.serialize() + SERIALIZE_SEP;

  // remove last SERIALIZE_SEP char
  ret = ret.substring(0, ret.length - 1);
  return ret;
}

List<UserModel> deserializeUsersList(String s) {
  if (s.length == 0) throw FormatException("Empty serialized Users list");

  var chunks = s.split(SERIALIZE_SEP);
  if (chunks.length % UserModel.N_SERIALIZED_CHUNKS != 0)
    throw FormatException("Invalid number of chunks for serialized Users list");

  int nUsers = (chunks.length / UserModel.N_SERIALIZED_CHUNKS).floor();
  return List<UserModel>.generate(
      nUsers,
      (i) => UserModel.deserializeChunks(chunks.sublist(
          i * UserModel.N_SERIALIZED_CHUNKS,
          (i + 1) * UserModel.N_SERIALIZED_CHUNKS)));
}
