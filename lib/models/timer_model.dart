import 'dart:async';
import 'dart:core';

import 'package:click_league/constants.dart';

class TimerModel {
  final _refreshInterval = Duration(milliseconds: 100);

  final int _durationMilliseconds;
  final int _countdownDurationMilliseconds = COUNTDOWN_DURATION * 1000;
  int _remainingMilliseconds = 0;
  int _countdownMilliseconds = 0;

  // Stopwatches count elapsed time
  final _countdownStopwatch = Stopwatch();
  final _stopwatch = Stopwatch();
  // StreamControllers hold streams to send the remaining time
  final _countdownStreamController = StreamController<int>();
  final _timerStreamController = StreamController<int>();
  // Completers notify when timers are over
  final _countdownFinishedCompleter = Completer();
  final _timerFinishedCompleter = Completer();



  TimerModel(int durationSeconds)
      : _durationMilliseconds = durationSeconds * 1000 {
    _updateCountdownRemainingMilliseconds();
    _updateRemainingMilliseconds();
  }

  int get durationSeconds => (_durationMilliseconds / 1000).round();

  Stream<int> get countdownStream => _countdownStreamController.stream;

  Future get countdownFinished => _countdownFinishedCompleter.future;

  Stream<int> get gameTimerStream => _timerStreamController.stream;

  Future get gameTimerFinished => _timerFinishedCompleter.future;

  bool get finished => _timerFinishedCompleter.isCompleted;

  void startCountdown() {
    _countdownStopwatch.reset();
    _startCountdownRefreshTimer();
    _countdownStopwatch.start();
  }

  void _startCountdownRefreshTimer() {
    _updateCountdownRemainingMilliseconds();
    Timer.periodic(_refreshInterval, (timer) {
      _updateCountdownRemainingMilliseconds();

      if (_countdownMilliseconds == 0) {
        timer.cancel();
        _countdownFinishedCompleter.complete(); // Notifies Countdown end
        _startGameTimer();
      }
    });
  }

  void _updateCountdownRemainingMilliseconds() {
    _countdownMilliseconds = _countdownDurationMilliseconds -
        _countdownStopwatch.elapsedMilliseconds;

    if (_countdownMilliseconds < 0)
      _countdownMilliseconds = 0;

    if (!_countdownStreamController.isClosed &&
        _countdownStreamController.hasListener)
      _countdownStreamController.add(_countdownMilliseconds);
  }

  void _startGameTimer() {
    _stopwatch.reset();
    _startRefreshTimer();
    _stopwatch.start();
  }

  void _startRefreshTimer() {
    _updateRemainingMilliseconds();
    Timer.periodic(_refreshInterval, (timer) {
      _updateRemainingMilliseconds();

      if (_remainingMilliseconds == 0) {
        timer.cancel();
        _timerFinishedCompleter.complete(); // Notifies timer end
      }
    });
  }

  void _updateRemainingMilliseconds() {
    _remainingMilliseconds =
        _durationMilliseconds - _stopwatch.elapsedMilliseconds;

    if (_remainingMilliseconds < 0)
      _remainingMilliseconds = 0;

    if (!_timerStreamController.isClosed && _timerStreamController.hasListener)
      _timerStreamController.add(_remainingMilliseconds);
  }

  void dispose() {
    if (!_timerStreamController.isClosed) _timerStreamController.close();
    if (!_countdownStreamController.isClosed)
      _countdownStreamController.close();
  }
}
