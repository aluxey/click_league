import 'package:flutter/material.dart';

import 'package:click_league/style.dart';
import 'package:click_league/views/main_menu.dart';

void main() => runApp(MyApp());


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Click League',
      initialRoute: '/main-menu',
      routes: {
        '/main-menu': (context) => MainMenu(),
      },
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: FontFamilyDefault,
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
          color: Colors.black,
        ),
        textTheme: TextTheme(
          headline1: Title1TextStyle,
          headline3: Title3TextStyle,
          subtitle1: SubtitleTextStyle,
          bodyText1: BodyTextStyle,
          bodyText2: BodyTextBoldStyle,
        ),
        primaryColor: Colors.green, // DOES NOT CHANGE ANYTHING
      ),
    );
  }
}



