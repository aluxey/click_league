import 'package:click_league/controllers/game_controller.dart';
import 'package:click_league/controllers/network/network_controller.dart';

abstract class HostController implements NetworkController {
  // When implementing `advertiseGame`, add `async` before the brackets:
  // async yields Future, async* yields Stream
  Future<void> advertiseGame(GameController gameController);

  void stopAdvertisingGame();

  void startGame();
}

mixin HostControllerMixin implements HostController {
  @override
  NetworkRole get role => NetworkRole.HOST;
}