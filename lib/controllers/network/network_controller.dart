import 'package:click_league/controllers/network/nearby/nearby_host_controller.dart';
import 'package:click_league/controllers/network/nearby/nearby_guest_controller.dart';

enum NetworkChannelType { NEARBY, BLUETOOTH, WIFI }

const Map<NetworkChannelType, String> NetworkChannelTypeString = {
  NetworkChannelType.NEARBY: "Google Nearby",
  NetworkChannelType.BLUETOOTH: "Bluetooth (not impl.)",
  NetworkChannelType.WIFI: "Wi-Fi (not impl.)",
};

enum NetworkRole { HOST, GUEST }

const Map<NetworkRole, String> NetworkRoleString = {
  NetworkRole.HOST: "Host",
  NetworkRole.GUEST: "Guest",
};

abstract class NetworkController {
  factory NetworkController(NetworkChannelType channel, NetworkRole role) {
    switch (channel) {
      case NetworkChannelType.NEARBY:
        switch (role) {
          case NetworkRole.GUEST:
            return NearbyGuestController();
          case NetworkRole.HOST:
            return NearbyHostController();
        }
      case NetworkChannelType.WIFI:
      case NetworkChannelType.BLUETOOTH:
        throw UnimplementedError();
    }
  }

  NetworkChannelType get channelType;
  NetworkRole get role;

  Future<bool> acquirePermissions();

  void endGame();

  void clearConnectedEndpoints();

  void dispose();
}

