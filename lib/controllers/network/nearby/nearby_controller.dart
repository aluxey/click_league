import 'dart:async';
import 'package:nearby_connections/nearby_connections.dart';
import 'package:click_league/controllers/network/network_controller.dart';

const NEARBY_NETWORK_STRATEGY = Strategy.P2P_CLUSTER;

enum NearbyNetworkState { DISCOVERING, AWAITING, PLAYING, ENDING }

mixin NearbyControllerMixin implements NetworkController {
  @override
  NetworkChannelType get channelType => NetworkChannelType.NEARBY;

  @override
  Future<bool> acquirePermissions() async {
    bool locationGranted = await Nearby().checkLocationPermission();
    if (!locationGranted) {
      locationGranted = await Nearby().askLocationPermission();
    }
    bool locationEnabled = await Nearby().checkLocationEnabled();
    if (!locationEnabled) {
      locationEnabled = await Nearby().enableLocationServices();
    }

    return locationEnabled && locationGranted;
  }

  @override
  void clearConnectedEndpoints() => Nearby().stopAllEndpoints();
}


