import 'dart:async';
import 'dart:collection';
import 'dart:math' show Random;

import 'package:flutter/services.dart';
import 'package:nearby_connections/nearby_connections.dart';

import 'package:click_league/constants.dart';
import 'package:click_league/utils.dart';
import 'package:click_league/controllers/game_controller.dart';
import 'package:click_league/controllers/network/guest_controller.dart';
import 'package:click_league/controllers/network/nearby/nearby_controller.dart';
import 'package:click_league/models/models.dart';

class NearbyGuestController
    with NearbyControllerMixin, GuestControllerMixin
    implements GuestController {
  static const _uploadUserInterval = Duration(milliseconds: 100);

  String _myEndpointId;

  late GameController _gameController;
  late VoidCallback _onGameStarts, _onGameJoined;

  // Predictable order
  final _endpointIdToGameDescription = LinkedHashMap<String, GameDescription>();
  final _availableGamesController =
      StreamController<Map<String, GameDescription>>();
  String? _hostEndpointId;

  var _state = NearbyNetworkState.DISCOVERING;

  Stream<Map<String, GameDescription>> get availableGamesStream =>
      _availableGamesController.stream;

  NearbyGuestController()
      : _myEndpointId = Random().nextInt(1 << 32).toString();

  @override
  Future<void> discoverGames(GameController gameController) async {
    _gameController = gameController;

    // Reset
    clearConnectedEndpoints();
    _endpointIdToGameDescription.clear();

    try {
      await Nearby().startDiscovery(_myEndpointId, NEARBY_NETWORK_STRATEGY,
          serviceId: NETWORK_SERVICE_ID, // uniquely identifies your app
          onEndpointFound: _onEndpointFound,
          onEndpointLost: _removeGameDescription);
    } catch (e) {
      // platform exceptions like unable to start bluetooth or insufficient permissions
      // showSnackbar(context, "Failed discovering: $exception");
      print("Failed discovering: $e");
    }
  }

  @override
  Future<void> joinGame(String endpointId,
      {required VoidCallback onGameJoined,
      required VoidCallback onGameStarts}) async {
    _onGameJoined = onGameJoined;
    _onGameStarts = onGameStarts;

    if (!_endpointIdToGameDescription.containsKey(endpointId))
      throw Exception('Endpoint $endpointId not in available games');

    try {
      await Nearby().requestConnection(_myEndpointId, endpointId,
          onConnectionInitiated: _onConnectionInitiated,
          onConnectionResult: _onConnectionResult,
          onDisconnected: (String id) =>
              print("Disconnected from endpoint $id"));
    } on PlatformException catch (e) {
      print("Platform exception : $e");
    }
  }

  @override
  void endGame() {
    _state = NearbyNetworkState.ENDING;
    _uploadUser();
  }

  @override
  void stopDiscoveringGames() => Nearby().stopDiscovery();

  @override
  void dispose() {
    stopDiscoveringGames();
    clearConnectedEndpoints();
    _endpointIdToGameDescription.clear();
    if (!_availableGamesController.isClosed) _availableGamesController.close();
  }

  void _doJoinGame(String endpointId) {
    if (!_endpointIdToGameDescription.containsKey(endpointId))
      throw Exception('Endpoint $endpointId not in available games');

    _state = NearbyNetworkState.AWAITING;
    _hostEndpointId = endpointId;
    var gd = _endpointIdToGameDescription[endpointId];

    _endpointIdToGameDescription.clear();
    stopDiscoveringGames();

    _gameController.initialiseTimer(gd!.gameDuration);

    // Send myUser
    var message = NetworkMessage.fromString(
            NetworkMessageType.USER, _gameController.myUser.serialize())
        .serialize();
    Nearby().sendBytesPayload(endpointId, message);
    _onGameJoined();
  }

  void _startGame() {
    _state = NearbyNetworkState.PLAYING;

    Timer.periodic(_uploadUserInterval, (timer) {
      if (_gameController.finished)
        timer.cancel();
      else
        _uploadUser();
    });
    _onGameStarts();
  }

  void _uploadUser() {
    assert(_hostEndpointId != null);

    var messType = (_state == NearbyNetworkState.ENDING)
        ? NetworkMessageType.FINAL_USER
        : NetworkMessageType.USER;
    var message =
        NetworkMessage.fromString(messType, _gameController.myUser.serialize())
            .serialize();
    Nearby().sendBytesPayload(_hostEndpointId!, message);
  }

  void _receiveFinalResults(List<UserModel> l) {
    print("Final results received!");

    for (final u in l) _gameController.setRemoteUser(u);

    // Triggers insertion of scores in DB
    _gameController.onFinalResultsReceived();
  }

  void _addGameDescription(String endpointId, GameDescription gd) {
    if (!_endpointIdToGameDescription.containsKey(endpointId)) {
      _endpointIdToGameDescription[endpointId] = gd;
      _availableGamesController.add(_endpointIdToGameDescription);
    }
  }

  void _removeGameDescription(String? endpointId) {
    if (endpointId != null &&
        _endpointIdToGameDescription.containsKey(endpointId)) {
      _endpointIdToGameDescription.remove(endpointId);
      _availableGamesController.add(_endpointIdToGameDescription);
    }
  }

  void _onEndpointFound(String endpointId, String userName, String serviceId) {
    if (serviceId != NETWORK_SERVICE_ID) {
      print(
          "Found endpoint with wrong service id. Expected $NETWORK_SERVICE_ID, got $serviceId");
      return;
    }

    // called when an advertiser is found
    GameDescription? gd;
    try {
      gd = GameDescription.deserialize(userName);
      _addGameDescription(endpointId, gd);
    } on FormatException catch (e) {
      print("Failed deserializing GameDescription: $e");
    }
  }

  Future<void> _onConnectionInitiated(
      String endpointId, ConnectionInfo info) async {
    if (!_endpointIdToGameDescription.containsKey(endpointId))
      throw Exception('Endpoint $endpointId not in available games');

    await Nearby()
        .acceptConnection(endpointId, onPayLoadRecieved: _onPayloadReceived);
  }

  void _onConnectionResult(String endpointId, Status status) {
    switch (status) {
      case Status.CONNECTED:
        _doJoinGame(endpointId);
        break;
      case Status.REJECTED:
      case Status.ERROR:
        throw Exception("Failed connecting to endpoint $endpointId");
    }
  }

  void _onPayloadReceived(String endpointId, Payload payload) {
    if (payload.type != PayloadType.BYTES || payload.bytes == null) return;

    var message = NetworkMessage.deserialize(payload.bytes!);

    // print("Received ${message.type} while $_state");

    if (_state == NearbyNetworkState.AWAITING &&
        message.type == NetworkMessageType.USERS_LIST) {
      List<UserModel> l = message.payload;
      for (final u in l) _gameController.setRemoteUser(u);
    } else if (_state == NearbyNetworkState.AWAITING &&
        message.type == NetworkMessageType.PLAY) {
      _startGame();
    } else if (_state == NearbyNetworkState.PLAYING &&
        message.type == NetworkMessageType.USERS_LIST) {
      List<UserModel> l = message.payload;
      for (final u in l) _gameController.setRemoteUser(u);
    } else if (_state == NearbyNetworkState.ENDING &&
        message.type == NetworkMessageType.FINAL_USERS_LIST) {
      _receiveFinalResults(message.payload);
    }
  }
}
