import 'dart:async';
import 'package:nearby_connections/nearby_connections.dart';

import 'package:click_league/constants.dart';
import 'package:click_league/controllers/game_controller.dart';
import 'package:click_league/controllers/network/host_controller.dart';
import 'package:click_league/controllers/network/nearby/nearby_controller.dart';
import 'package:click_league/models/models.dart';

class NearbyHostController
    with NearbyControllerMixin, HostControllerMixin
    implements HostController {
  static const _usersListBroadcastInterval = Duration(milliseconds: 100);
  static const _sendFinalUsersListTimeout = Duration(seconds: 3);

  late GameController _gameController;

  final Map<String, int> _endpointToUserId = {};

  Map<int, bool>? _finalUserResultReceived;
  var _finalUsersListWasBroadcast = false;

  var _state = NearbyNetworkState.AWAITING;

  @override
  Future<void> advertiseGame(GameController gameController) async {
    // TODO? Check that gameController did not already exist
    _gameController = gameController;
    var myEndpointId = GameDescription(gameController.myUser.id,
            gameController.myUser.userName, gameController.gameDuration)
        .serialize();

    try {
      await Nearby().startAdvertising(
        myEndpointId,
        NEARBY_NETWORK_STRATEGY,
        serviceId: NETWORK_SERVICE_ID, // uniquely identifies your app
        onConnectionInitiated: _onConnectionInitiated,
        onConnectionResult: _onConnectionResult,
        onDisconnected: _onDisconnected,
      );
    } catch (exception) {
      // platform exceptions like unable to start bluetooth or insufficient permissions
      print("Failed advertising: $exception");
    }
  }

  @override
  void startGame() {
    _state = NearbyNetworkState.PLAYING;
    stopAdvertisingGame();
    _broadcastPlay();
    Timer.periodic(_usersListBroadcastInterval, (timer) {
      if (_gameController.finished)
        timer.cancel();
      else
        _broadcastUsersList();
    });
  }

  @override
  void endGame() {
    _state = NearbyNetworkState.ENDING;
    _initEndGame();
  }

  @override
  void stopAdvertisingGame() => Nearby().stopAdvertising();

  @override
  void dispose() {
    _endpointToUserId.clear();
    stopAdvertisingGame();
    clearConnectedEndpoints();
  }

  Future<void> _broadcastUsersList({bool isFinal: false}) async {
    var messType = isFinal
        ? NetworkMessageType.FINAL_USERS_LIST
        : NetworkMessageType.USERS_LIST;
    var message = NetworkMessage.fromString(
            messType, serializeUsersList(_gameController.users))
        .serialize();

    await Future.wait(_endpointToUserId.keys
        .map((endpointId) => Nearby().sendBytesPayload(endpointId, message)));
  }

  Future<void> _broadcastPlay() async {
    var message = NetworkMessage.PLAY.serialize();
    await Future.wait(_endpointToUserId.keys
        .map((endpointId) => Nearby().sendBytesPayload(endpointId, message)));
  }

  void _initEndGame() {
    if (_finalUsersListWasBroadcast || _finalUserResultReceived != null) return;

    // We need to wait for every FINAL_USER from the remotes to know the final scores.
    // We use a map of User.id <-> bool for this:
    _finalUserResultReceived = Map<int, bool>.fromIterable(
        _gameController.users.map((u) => u.id),
        value: (_) => false);
    // We know our own final score
    _finalUserResultReceived![_gameController.myUser.id] = true;
    // Set the timeout to force sending FINAL_USERS_LIST
    // even if some FINAL_USER are missing
    Timer(_sendFinalUsersListTimeout, _broadcastFinalResults);
  }

  void _receiveFinalUser(UserModel u) {
    if (_finalUsersListWasBroadcast) return;
    if (_finalUserResultReceived == null) {
      _initEndGame();
    }

    _gameController.setRemoteUser(u);
    _finalUserResultReceived![u.id] = true;

    // Did all users send their final results?
    var allFinalResultsReceived =
        _finalUserResultReceived!.values.every((e) => e);
    if (allFinalResultsReceived) _broadcastFinalResults();
  }

  void _broadcastFinalResults() {
    if (_finalUsersListWasBroadcast) return;

    // gameState now contains the final results: send it to the stream & close
    // _finalGameStateStream.add(gameState);
    // _finalGameStateStream.close();

    print("Broadcasting final results!");
    _broadcastUsersList(isFinal: true);
    _finalUsersListWasBroadcast = true;
    // Triggers insertion of scores in DB
    _gameController.onFinalResultsReceived();
    // dispose();
  }

  void _removeUserAndBroadcast(String endpointId) {
    if (_endpointToUserId.containsKey(endpointId) &&
        _endpointToUserId[endpointId] != 0) {
      _gameController.removeRemoteUser(_endpointToUserId[endpointId]!);
      _endpointToUserId.remove(endpointId);
      Nearby().disconnectFromEndpoint(endpointId);
      _broadcastUsersList();
    }
  }

  // Nearby callbacks

  void _onConnectionInitiated(String endpointId, ConnectionInfo info) {
    // Called whenever a discoverer requests connection

    if (!_endpointToUserId.containsKey(endpointId)) {
      // We don't know the remote UserId yet, so we put 0.
      _endpointToUserId[endpointId] = 0;
      print("Successfully added $endpointId to _endpointToUserId");
    } else {
      print(
          "Connection already initiated to $endpointId with Id ${_endpointToUserId[endpointId]}");
    }

    Nearby()
        .acceptConnection(endpointId, onPayLoadRecieved: _onPayloadReceived);
  }

  void _onConnectionResult(String endpointId, Status status) {
    // Called when connection is accepted/rejected
    switch (status) {
      case Status.CONNECTED:
        assert(_endpointToUserId.containsKey(endpointId));
        break;
      case Status.ERROR:
      case Status.REJECTED:
        _removeUserAndBroadcast(endpointId);
        break;
    }
  }

  void _onDisconnected(String id) => _removeUserAndBroadcast(id);

  void _onPayloadReceived(String endpointId, Payload payload) {
    if (payload.type != PayloadType.BYTES || payload.bytes == null) return;

    if (!_endpointToUserId.containsKey(endpointId)) {
      print("Payload from unknown user $endpointId");
      return;
    }

    var message = NetworkMessage.deserialize(payload.bytes!);

    // print("Received ${message.type} while $_state");

    if (_state == NearbyNetworkState.AWAITING &&
        message.type == NetworkMessageType.USER) {
      UserModel u = message.payload;
      // Only add & broadcast users that were not already initialised
      if (_endpointToUserId[endpointId] == 0) {
        _endpointToUserId[endpointId] = u.id;
        _gameController.setRemoteUser(u);
        _broadcastUsersList();
      }
    } else if (_state == NearbyNetworkState.PLAYING &&
        message.type == NetworkMessageType.USER) {
      UserModel u = message.payload;
      // We don't wanna evil Users giving someone else's score!!
      // While playing, we periodically broadcast the users list:
      // no need to call _broadcastUsersList manually
      if (u.id == _endpointToUserId[endpointId])
        _gameController.setRemoteUser(u);
    } else if (_state == NearbyNetworkState.ENDING &&
        message.type == NetworkMessageType.FINAL_USER) {
      UserModel u = message.payload;
      if (u.id == _endpointToUserId[endpointId]) {
        _receiveFinalUser(u);
      }
    }
  }
}
