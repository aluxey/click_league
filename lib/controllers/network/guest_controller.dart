import 'package:click_league/utils.dart';
import 'package:click_league/controllers/network/network_controller.dart';
import 'package:click_league/controllers/game_controller.dart';
import 'package:click_league/models/game_description.dart';


abstract class GuestController implements NetworkController {
  // When implementing `discoverGames`, add `async*` before the brackets:
  // async yields Future, async* yields Stream
  Future discoverGames(GameController gameController);

  Stream<Map<String, GameDescription>> get availableGamesStream;

  void stopDiscoveringGames();

  Future<void> joinGame(String endpointId,
      {required VoidCallback onGameJoined,
      required VoidCallback onGameStarts});
}

mixin GuestControllerMixin implements GuestController {
  @override
  NetworkRole get role => NetworkRole.GUEST;
}