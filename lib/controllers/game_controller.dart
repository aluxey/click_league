import 'package:flutter/foundation.dart';

import 'package:click_league/controllers/network/host_controller.dart';
import 'package:click_league/controllers/network/network_controller.dart';
import 'package:click_league/controllers/users_controller.dart';
import 'package:click_league/models/models.dart';

class GameController {
  final NetworkController? _netController;
  late final TimerModel _gameTimer;
  late final UsersController _usersController;
  final _highScoresModel = HighScoresModel();

  GameController({NetworkController? netController})
      : _netController = netController;

  void initialise(int gameDuration, String myUserName) {
    initialiseTimer(gameDuration);
    initialiseUser(myUserName);
  }

  void initialiseTimer(int gameDuration) =>
      _gameTimer = TimerModel(gameDuration);

  void initialiseUser(String myUserName) =>
      _usersController = UsersController(myUserName);

  void dispose() {
    _gameTimer.dispose();
    _usersController.dispose();
  }

  int get gameDuration => _gameTimer.durationSeconds;

  UserModel get myUser => _usersController.myUser;

  // Always returns myUser first
  List<UserModel> get users => _usersController.users;

  // Always returns myUser first
  Stream<List<UserModel>> get usersStream => _usersController.usersStream;

  // Timer streams are in milliseconds
  Stream<int> get gameTimerStream => _gameTimer.gameTimerStream;

  Stream<int> get countdownStream => _gameTimer.countdownStream;

  Future get countdownFinished => _gameTimer.countdownFinished;

  Future get gameTimerFinished => _gameTimer.gameTimerFinished;

  bool get finished => _gameTimer.finished;

  Future get finalResultsReceived => _usersController.finalResultsReceived;

  bool get isMultiplayer => _netController != null;

  bool get isHost => isMultiplayer && netController.role == NetworkRole.HOST;

  NetworkController get netController {
    assert(_netController != null);
    return _netController!;
  }

  void startGame({required VoidCallback onGameFinished}) {
    if (isHost) (netController as HostController).startGame();

    _gameTimer.startCountdown();

    gameTimerFinished.whenComplete(() {
      print("Game finished!!!");
      if (isMultiplayer) {
        netController.endGame();
      } else {
        // In multiplayer mode, the netController calls this callback.
        // In singleplayer, gameController needs to do it here.
        onFinalResultsReceived();
      }

      // onGameFinished is defined in GameView and changes the view to GameResultsView
      // => no flutter import in controllers
      onGameFinished();
    });

    // Insert final results in high scores once received
    _usersController.finalResultsReceived.whenComplete(
        () => _highScoresModel.insertScores(users, gameDuration, myUser.id));
  }

  void onGameButtonPressed() {
    if (!finished) {
      _usersController.incrementMyScore();
    }
  }

  void setRemoteUser(UserModel u) => _usersController.setRemoteUser(u);

  void removeRemoteUser(int id) => _usersController.removeRemoteUser(id);

  void onFinalResultsReceived() {
    print("Notifying of final results.");
    _usersController.onFinalResultsReceived();
  }
}
