import 'dart:async';
import 'dart:collection';

import 'package:click_league/models/user_model.dart';

// TODO: sort users by scores and trigger _sendUsersToStream if order changed

class UsersController {

  final UserModel _myUser;
  final _remoteUsers = LinkedHashMap<int, UserModel>(); // Ordered
  // StreamControllers hold streams to send Users' list
  final _usersStreamController = StreamController<List<UserModel>>.broadcast();
  // Completers notify when final results are received
  final _resultsReceivedCompleter = Completer();

  UsersController(String myUserName)
      : _myUser = UserModel.fromUserName(myUserName);

  UserModel get myUser => _myUser;

  // Always returns myUser first
  List<UserModel> get users {
    List<UserModel> users = [_myUser];
    users.addAll(_remoteUsers.values);
    return users;
  }

  Stream<List<UserModel>> get usersStream => _usersStreamController.stream;

  Future get finalResultsReceived => _resultsReceivedCompleter.future;

  void incrementMyScore() => _myUser.incrementScore();

  void setRemoteUser(UserModel u) {
    // Do not add myUser as a remote user
    // (The host notably sends all scores including my own)
    if (u.id == _myUser.id) return;

    if (_remoteUsers.containsKey(u.id)) {
      // If User already exists, simply update its score (will be piped down stream)
      _remoteUsers[u.id]!.setScore(u.score);
    } else {
      // Else, add it and push the users list down the stream
      _remoteUsers[u.id] = u;
      _sendUsersToStream();
    }
  }

  void removeRemoteUser(int id) {
    if (_remoteUsers.containsKey(id)) {
      _remoteUsers.remove(id);
      _sendUsersToStream();
    }
  }

  void onFinalResultsReceived() => _resultsReceivedCompleter.complete();

  void dispose() {
    _myUser.dispose();
    for (var u in _remoteUsers.values) u.dispose();
    if (!_usersStreamController.isClosed) _usersStreamController.close();
  }

  void _sendUsersToStream() {
    // Triggers refresh of the whole ScoresSection
    if (_usersStreamController.hasListener)
      _usersStreamController.add(users);
  }
}
