import 'package:flutter/material.dart';

void showSnackbar(BuildContext context, dynamic a) {
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    content: Text(a.toString()),
  ));
}

typedef VoidCallback = void Function();