import "package:flutter/material.dart";

const TitleTextSize = 32.0;
const LargeTextSize = 26.0;
const MediumTextSize = 18.0;
const BodyTextSize = 16.0;

const String FontFamilyDefault = "JosefinSans";
const String PixelFontFamily = "PressStart2P";

const Title1TextStyle = TextStyle(
  fontFamily: PixelFontFamily,
  fontWeight: FontWeight.w400,
  fontSize: TitleTextSize,
  color: Colors.green,
);

const Title3TextStyle = TextStyle(
  fontFamily: PixelFontFamily,
  fontWeight: FontWeight.w400,
  fontSize: MediumTextSize,
  color: Colors.white,
);

const SubtitleTextStyle = TextStyle(
  fontFamily: FontFamilyDefault,
  fontWeight: FontWeight.w300,
  fontSize: MediumTextSize,
  color: Colors.white,
);

const BodyTextStyle = TextStyle(
  fontFamily: FontFamilyDefault,
  fontWeight: FontWeight.w300,
  fontSize: BodyTextSize,
  color: Colors.white,
);


const BodyTextBoldStyle = TextStyle(
  fontFamily: FontFamilyDefault,
  fontWeight: FontWeight.w400,
  fontSize: BodyTextSize + 2,
  color: Colors.white,
);