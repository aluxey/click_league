import 'package:test/test.dart';

import 'package:click_league/models/user_model.dart';

void main() {
  group("Serialization of UserModel", () {
    final user = UserModel(50, "ImATest", 55);
    final users = [user, UserModel(33, "ARealHero", 33)];

    test("UserModel serialisation", () {
      String buf = user.serialize();
      final deserializedUser = UserModel.deserialize(buf);
      expect(deserializedUser.id, equals(user.id));
      expect(deserializedUser.userName, equals(user.userName));
      expect(deserializedUser.score, equals(user.score));
    });

    test("List<UserModel> serialisation", () {
      String buf = serializeUsersList(users);
      final deserializedUsers = deserializeUsersList(buf);

      expect(deserializedUsers.length, equals(users.length));
      for (int i = 0; i < users.length; i++) {
        expect(deserializedUsers[i].id, equals(users[i].id));
        expect(deserializedUsers[i].userName, equals(users[i].userName));
        expect(deserializedUsers[i].score, equals(users[i].score));
      }
    });
  });
}
